# Feature Flags Interview

## About the problem

Create the logic to handle feature flags in a React Application.

The user can opt-in to turn on|off the feature flags, this state should save
locally.

### Business Rules

1. A feature appears if the its flag value is `active=true` or if the local
   value of the flag is `true`.
2. A Feeatue is considered released when its flag doesn't exist, in the case the
   Feature should always appear.
3. An active feature can't be deactivated.

### Data model

Feature flags are given by the model:

```typescript
FeatureFlag {
  string name
  boolean active
}
```

## Goal

To demostrate your algoritmic skills and your
knowladge of TS/JS programing lenguajes.

Demostrating your React knoladge is a plus!

## Steps

Complete as many steps as you can. Happy Coding!

### Step 0

Start the project by using `npm start`.

### Step 1

In the `App.tsx` file, load the feature flags using service function
`new FeatureFlagService().getAll()`, and save them in local storage.

### Step 2

In the `feature-flag.utils.ts` file, update the `ff` funciton to return if the
flag is active or not.

### Step 3

**3.1:** In the `App.tsx` file, pass the loaded feature flags to the
`FeatureFlags` component.

**3.2:** In the `FeatureFlags.tsx` file, finish the compnent to render the flags
availabe using the `FeatureFlag` component.

### Step 4

In the `FeatureFlag.tsx` file, Handle checkbox's onChange event to set the
active state of it.

### Step 5

In the `FeatureFlag.tsx` file, update the local value of the flag, this value
will replace the incoming value from the service.

### Step 6

In the `App.tsx` file, update the feature flags with local value if applicable.
