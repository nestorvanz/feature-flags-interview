import React, { useEffect, useMemo, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { FeatureFlagService } from "./feature-flag/feature-flag.service";
import Feature from "./feature/Feature";
import FeatureFlags from "./feature-flag/FeatureFlags";
import { IFeatureFlag } from "./feature-flag/feature-flag.internface";

function Inintializing() {
  return <div>Initializing application...</div>;
}

function App() {
  const [loading, setLoading] = useState(true);

  // STEP 3.1.1 STARTS *********************************************************
  const [featureFlags, setFeatureFlags] = useState<
    IFeatureFlag[] | undefined
  >();
  // STEP 3.1.1 ENDS ===========================================================

  // STEP 1 STARTS *************************************************************
  useEffect(() => {
    new FeatureFlagService()
      .getAll()
      .then((data) => {
        const flagsMap: { [flag: string]: boolean } = {};
        data.forEach((flag) => {
          flagsMap[flag.name] = flag.active;
        });
        localStorage.setItem("feature_flags", JSON.stringify(flagsMap));

        // STEP 6 STARTS *******************************************************
        const localFlagsMap = JSON.parse(
          localStorage.getItem("local_feature_flags") ?? "{}"
        );
        data.forEach((flag) => {
          if (flag.active === false) {
            flag.active = localFlagsMap[flag.name];
          }
          flagsMap[flag.name] = flag.active;
        });
        localStorage.setItem("feature_flags", JSON.stringify(flagsMap));
        // STEP 6 ENDS =========================================================

        // STEP 3.1.2 STARTS ***************************************************
        setFeatureFlags(data);
        // STEP 3.1.2 ENDS =====================================================
      })
      .finally(() => {
        setLoading(false);
      });
  });
  // STEP 1 ENDS ===============================================================

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {loading && <Inintializing />}
        {!loading && (
          <>
            <Feature name="1" flag="one" />
            <Feature name="2" flag="two" />
            <Feature name="3" flag="three" />
          </>
        )}

        {featureFlags && <FeatureFlags flags={featureFlags} />}
      </header>
    </div>
  );
}

export default App;
