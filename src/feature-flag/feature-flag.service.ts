import { FEATURE_FLAG_DATA } from "./feature-flag.data";
import { IFeatureFlag } from "./feature-flag.internface";

export class FeatureFlagService {
  getAll(): Promise<IFeatureFlag[]> {
    return new Promise((resolve) => {
      const min = 1000;
      const max = 3000;
      const ms = Math.random() * (max - min) + min;
      setTimeout(() => {
        resolve(FEATURE_FLAG_DATA);
      }, ms);
    });
  }
}
