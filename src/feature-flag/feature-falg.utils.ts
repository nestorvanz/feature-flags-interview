export function ff(flag: string): boolean {
  // STEP 2 STARTS *************************************************************
  const featureFlags = JSON.parse(
    localStorage.getItem("feature_flags") ?? "{}"
  );
  return featureFlags[flag] ?? true;
  // STEP 2 ENDS ===============================================================

  // return true; // Default value
}
