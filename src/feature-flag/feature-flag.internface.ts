export interface IFeatureFlag {
  name: string;
  active: boolean;
}
