import { IFeatureFlag } from "./feature-flag.internface";

export const FEATURE_FLAG_DATA: IFeatureFlag[] = [
  { name: "one", active: false },
  { name: "two", active: false },
  { name: "three", active: false },
];
