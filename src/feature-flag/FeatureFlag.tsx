import React, { useCallback, useState } from "react";
import { IFeatureFlag } from "./feature-flag.internface";

type Props = {
  flag: IFeatureFlag;
};

function FeatureFlag({ flag }: Props) {
  const [active, setActive] = useState(flag.active);
  // HINT: event: React.ChangeEvent<HTMLInputElement>

  // STEP 4 STARTS *************************************************************
  const handlerChecked = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, flag: IFeatureFlag) => {
      setActive(event.target.checked);

      // STEP 5 STARTS *********************************************************
      const localFeatureFlagsString =
        localStorage.getItem("local_feature_flags") ?? "{}";
      const localFeatureFlags = JSON.parse(localFeatureFlagsString);
      localFeatureFlags[flag.name] = event.target.checked;

      localStorage.setItem(
        "local_feature_flags",
        JSON.stringify(localFeatureFlags)
      );
      // STEP 5 ENDS ===========================================================
    },
    []
  );
  // STEP 4 ENDS ===============================================================

  return (
    <li>
      <input
        type="checkbox"
        checked={active}
        onChange={(event) => handlerChecked(event, flag)}
      />
      {flag.name}
    </li>
  );
}

export default FeatureFlag;
