import React from "react";
import { IFeatureFlag } from "./feature-flag.internface";
import FeatureFlag from "./FeatureFlag";

type Props = {
  flags: IFeatureFlag[];
};

function FeatureFlags({ flags }: Props) {
  return (
    <>
      <h2>Feature flags</h2>
      <ul>
        {/* STEP 3.1.2 STARTS ********************************************** */}
        {flags.map((flag, index) => {
          return <FeatureFlag key={index} flag={flag} />;
        })}
        {/* STEP 3.1.2 ENDS ================================================ */}
      </ul>
    </>
  );
}

export default FeatureFlags;
