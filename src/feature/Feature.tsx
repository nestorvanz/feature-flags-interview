import React from "react";
import { ff } from "../feature-flag/feature-falg.utils";

type Props = {
  name: string;
  flag: string;
};

function Feature({ name, flag }: Props) {
  if (!ff(flag)) {
    return <></>;
  }

  return (
    <div>
      <span>This is feature {name}</span>
    </div>
  );
}

export default Feature;
